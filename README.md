# docker-django

## build and push:

```
docker build -t registry.gitlab.com/dskvr/docker-django:3.7 -f 3.7/Dockerfile .
docker push registry.gitlab.com/dskvr/docker-django:3.7
```

```
docker build -t registry.gitlab.com/dskvr/docker-django:3.7-cyberfog -f 3.7-cyberfog/Dockerfile .
docker push registry.gitlab.com/dskvr/docker-django:3.7-cyberfog
```

## usage:
```
some-django-service:
    image: registry.gitlab.com/dskvr/docker-django:3.7
```

# update
```
docker-compose up --force-recreate --build -d
docker image prune -f
```

## wait-for-it.sh

https://github.com/Eficode/wait-for

script designed to synchronize services like docker containers. It is sh and alpine compatible. It was inspired by vishnubob/wait-for-it, but the core has been rewritten at Eficode by dsuni and mrako.

usage:

```
version: '2'

services:
  db:
    image: postgres:9.4

  backend:
    build: backend
    command: sh -c './wait-for db:5432 -- npm start'
    depends_on:
      - db
```